import devConfig from '../webpack.config.dev';
import prodConfig from '../webpack.config.prod';

export const isProduction = () => process.env.NODE_ENV === 'production';
export const getConfig = () => isProduction() ? prodConfig : devConfig;
