import express from 'express';
import { isProduction } from '../config';

var router = express.Router();

const apiFormat = (payload) => ({
    payload: payload
});

// define the home page route
router.get('/', function(req, res) {
    res.json(apiFormat({message: 'This is the api root and serves no purpose.'}));
});

export default router;