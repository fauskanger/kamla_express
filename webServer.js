import path from 'path';
import helmet from 'helmet'
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import Express from 'express';
import ExpressSession from 'express-session'
import { Server } from 'http'
import * as _ from 'lodash';

import apiRoute from './server/api'
import { isProduction, getConfig } from './server/config';


const port = 3000;
const app = new Express();
const http = Server(app);

const io = require('socket.io')(http);
const MongoDBStore = require('connect-mongodb-session')(ExpressSession);

const sessionStore = new MongoDBStore(
    {
        uri: 'mongodb://localhost:27017/kamla_mongo_db',
        collection: 'sessions'
    });

// Catch errors
sessionStore.on('error', function(error) {
    assert.ifError(error);
    assert.ok(false);
});

app.use(ExpressSession({
    secret: 'This is a secret',
    cookie: {
        maxAge: 1000 * 60 * 60 * 24 * 7 // 1 week
    },
    store: sessionStore,
    resave: false,
    saveUninitialized: false
}));

const config = getConfig();
const compiler = webpack(config);

console.log(config);

const compilerCallback = (err, stats)=>{
    if (err){
        console.log(`An error occured while running the webpack compiler!`);
        console.log(err);
    } else {
        console.log(`Bundle build complete`)
    }
};

if (isProduction()) {
    console.log('Starting to compile production bundle..');
    compiler.run(compilerCallback);
    // If true, the client’s IP address is understood as the left-most entry in the X-Forwarded-* header.
    app.set('trust proxy', true);
    app.use(helmet());
} else {
    const devMiddleware = webpackDevMiddleware(compiler, {
        noInfo: true,
        publicPath: config.output.publicPath,
        stats: {
            colors: true
        },
        watchOptions: {
            aggregateTimeout: 300, // Default: 300
        }
    });
    devMiddleware.waitUntilValid((stats)=>{
        console.log('Development build complete.');
        compilerCallback('', stats);
    });
    app.use(devMiddleware);
}

app.use('/static', Express.static('public'));

app.use('/api', apiRoute);

app.get('/fiddle', (req, res) => {
    res.sendFile(path.join(__dirname, 'src', 'fiddle', 'index.html'));
});

app.get('/manifest.json', (req, res) => {
    res.sendFile(path.join(__dirname, 'manifest.json'));
});

app.get('/*', (req, res) => {
    res.sendFile(path.join(__dirname, 'index.html'));
});

http.listen(port, error => {
    /* eslint-disable no-console */
    if (error) {
        console.error(error);
    } else {
        console.info(`🌎 Listening on port ${port}. Open up http://localhost:${port}/ in your browser.`);
        // this log returns undefined for config.mode - is it meant to be watch mode?
        console.log(`process.env.NODE_ENV=${process.env.NODE_ENV}`)
    }
    /* eslint-enable no-console */
});
