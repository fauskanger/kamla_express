const byId = (state = {}, action) => {
    let new_state = state;
    if (action.response) {
        new_state = {
            ...state,
            ...action.response.entities.todos,
        };
    }
    return new_state;
};

export default byId;

export const getTodo = (state, id) => state[id];
export const getTodoByIndex = (state, index, filter='all') => {
    // index can be negative as in Python slicing
    let ids = state.listByFilter[filter].ids;
    if (ids.length < 1 || ids.length <= index)
        return null;
    return getTodo(state.byId, ids.slice(index)[0])
};
export const getRandomTodo = (state, filter='all') => {
    let ids = state.listByFilter[filter].ids;
    if (ids.length <= index)
        return null;
    return getTodo(state.byId, ids.slice(index)[0])
};