import { UPDATE_USER } from '../actions/user';

let user = (user={}, action) => {
    switch (action.type) {
        case UPDATE_USER:
            console.log('UPDATE_USER: ' + JSON.stringify(action.payload));
            return {...user, ...action.payload};
        default:
            return user;
    }
};

export default user;

