import { combineReducers } from 'redux';
import byId, * as fromById from './byId';
import user, * as fromUser from './user';
import createList, * as fromList from './createList';
import { reducer as formReducer } from 'redux-form'

const listByFilter = combineReducers({
    all: createList('all'),
    active: createList('active'),
    completed: createList('completed'),
});

const highlightReducer = (state=null, action) =>
    (action.type && action.type=='UPDATE_HIGHLIGHT')?
        action.newHighlightId:
        state;

const root_reducer = combineReducers({
    byId,
    listByFilter,
    user,
    highlightId: highlightReducer,
    form: formReducer,
});

export default root_reducer;

export const getVisibleTodos = (state, filter) => {
    const ids = fromList.getIds(state.listByFilter[filter]);
    return ids.map(id => fromById.getTodo(state.byId, id));
};

export const getIsFetching = (state, filter) =>
    fromList.getIsFetching(state.listByFilter[filter]);

export const getErrorMessage = (state, filter) =>
    fromList.getErrorMessage(state.listByFilter[filter]);

export const getRandomTodoId = (todosById) => {
    let ids = Object.keys(todosById);
    return ids.length > 0 ?
        ids[parseInt(Math.random() * ids.length)] :
        null;
};