import * as d3 from 'd3';
import _ from "lodash";
import randomColor from "randomcolor";

console.log('--- --- ---  Kamλa  --- --- ---\n\n');

// Some helper functions
const factorial = (n) => {
    if (n < 1) return 1;
    let tot = n;
    while (--n > 1) {
        tot *= n;
    }
    return tot;
};
const range = x => [...Array(x).keys()];
const last = arr => arr[arr.length - 1];
const exp = x => Math.pow(Math.E, x);
const poisson = (lambda, k) => Math.pow(lambda, k) * exp(-lambda) / factorial(k);

// Setup data
let nSeries = 30;
let kRange = range(nSeries + 1);
let lRange = range(nSeries + 1).slice(1); // Skip lambda=0 as gives p(k=0)=1

// Configure plot pixel dimensions
let webMargin = {
    top: 30,
    right: 40,
    bottom: 30,
    left: 50,
};
let noMargin = {
    top: 0,
    right: 10,
    bottom: 0,
    left: 10
};
let margin = webMargin;
let width = 1080 - margin.left - margin.right;
let height = 720 - margin.top - margin.bottom;
let colors = randomColor({
    count: nSeries,
    color: 'bright',
    seed: 42 + parseInt(nSeries*3) + 1
});

// Create data
let data = {};
lRange.forEach((l,i) => {
    let d = {};
    data[l] = d;
    d.lambda = l;
    d.color = colors[i];
    d.points = kRange.map(k => ({
        k: k,
        prob: poisson(l, k)
    }));
});

// Prepare plot, axes
var decimalFormatter = d3.format(".2");

var x = d3.scaleLinear().range([0, width]);
var y = d3.scaleLinear().range([height, 0]);

var xAxis = d3.axisBottom(x)
    .ticks(kRange.length - 1);

var yAxis = d3.axisLeft(y)
    .ticks(5)
    .tickFormat(function(d) {
        d = decimalFormatter(d);
        return d * 100 + '%';
    });

let maxProb = d3.max(_.map(data, series => d3.max(series.points, d => d.prob)));

x.domain([d3.min(kRange), d3.max(kRange)]);
y.domain([0, maxProb+0.05]);

let valueline = d3.line()
    .x(d => x(d.k))
    .y(d => y(d.prob))
    .curve(d3.curveBasis);


let svg = d3.select("body")
    .append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

svg.append("g") // Add the X Axis
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis);

svg.append("g") // Add the Y Axis
    .attr("class", "y axis")
    .call(yAxis);

let seriesCounter = 0;
let renderPoints = (series, valueline, color) => {
    let points = series.points;
    let lambda = series.lambda;
    let iMaxProb = d3.scan(points, (a, b) => b.prob - a.prob);
    let labelPos = {
        x: x(lambda)-width/nSeries/2,
        y: y(points[iMaxProb].prob) - 20
    };

    // Scale the range of the data
    svg.append("path") // Add the valueline path.
        .style("stroke", color)
        .attr("d", valueline(points));


    console.log(labelPos.x + ' | ' + labelPos.y);

    svg.append("text")
        .attr("transform", "translate(" + labelPos.x + "," + labelPos.y + 0 + ")")
        .attr("dy", ".35em")
        .attr("text-anchor", "start")
        .style("fill", color)
        .text('λ = ' + lambda);
};

_.forEach(data, series => renderPoints(series, valueline, series.color));


