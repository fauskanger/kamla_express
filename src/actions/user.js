
// Actions
export const UPDATE_USER = 'UPDATE_USER';

export const updateUser = (newUserProperties=>({
    type: UPDATE_USER,
    payload: newUserProperties
}));