import React from 'react';
import * as _ from 'lodash'

// Adapted from https://www.sitepoint.com/testing-for-empty-values/
export const empty = (data) => {
    if(typeof(data) == 'number' || typeof(data) == 'boolean')
    {
        return false;
    }
    if(typeof(data) == 'undefined' || data === null)
    {
        return true;
    }
    if(typeof(data.length) != 'undefined')
    {
        return data.length == 0;
    }

    return data.every((d) => !data.hasOwnProperty(d));
    // for(var i in data)
    // {
    //     if(data.hasOwnProperty(i))
    //     {
    //         return false;
    //     }
    // }
    // return true;
};

// From http://stackoverflow.com/questions/2532218/pick-random-property-from-a-javascript-object
export const randomProperty = function (obj, filter_callback=null) {
    var keys = filter_callback? Object.keys(obj).filter(filter_callback): Object.keys(obj);
    return obj[keys[ keys.length * Math.random() << 0]];
};


// From https://toddmotto.com/methods-to-determine-if-an-object-has-a-given-property/
// Usage: if (hasProp(toddObject, 'favouriteDrink')) {}
export const hasProp =  (obj, prop) => obj && Object.prototype.hasOwnProperty.call(obj, prop);

export const getPropIfExists = (obj, prop, defVal={}) => hasProp(obj, prop)? obj[prop]: defVal;

// Take object and 'prettyprint it' with Boostrap horizontal description for key-values
// (See "Description" under http://getbootstrap.com/css/#type-lists )
export const renderObjectAsDescription = (obj, includeFalsy=false)=>{
    let propKey = 0;
    let renderProperty = (obj) => {
        if (_.isArray(obj)) {
            return _.isEmpty(obj) ?
                'Empty list'
                : _(obj)
                .map(element => renderProperty(element))
                .value()
        } else {
            switch (typeof obj) {
                case 'boolean':
                    return obj ? 'true' : 'false';
                case 'object':
                    return _.isEmpty(obj) ?
                        'Empty object'
                        : renderObjectAsDescription(obj);
                case 'function':
                    return `function: ${obj.name}`;
                default:
                    return obj;
            }
        }
    };
    let pairFromKey = (key)=> (!obj[key] && !includeFalsy)?
        false
        :<span key={propKey++}><dt>{key}</dt><dd>{renderProperty(obj[key])}</dd></span>;
    return (<dl className="dl-horizontal">
        {_.map(_.keys(obj), (key)=>pairFromKey(key) )}
    </dl>)
};

export const renderAsJSON = obj => _.isEmpty(obj)? '': <div><pre>{JSON.stringify(obj, null, 2) }</pre></div>;
export const isProduction = ()=> process.env.NODE_ENV === 'production';
