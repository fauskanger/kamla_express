import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import Root from './components/Root';
import configureStore from './configureStore';
import injectTapEventPlugin from 'react-tap-event-plugin';

// Needed for onTouchTap (for material-ui)
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();

const store = configureStore();

render(
  <Root store={store} />,
  document.getElementById('root')
);
