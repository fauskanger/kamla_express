import React from 'react'

export const getCardUrl = ({rank, suit}) => {
    let img_name = `${rank}${suit}${rank>10? '2': ''}`;
    return `https://atle.vedfjord.no/static/img/cards/svg/${img_name}.svg`;
};

export const getCardImg = ({rank, suit, ...rest}) => {
    return <img src={getCardUrl({rank, suit})} key={`${rank}${suit}`} {...rest}/>;
};
