import { createStore, applyMiddleware } from 'redux';
import { persistStore, autoRehydrate } from 'redux-persist'
import createLogger from 'redux-logger';
import thunk from 'redux-thunk';
import todoApp from './reducers';

let persistor = null;

const configureStore = () => {
    const middlewares = [thunk];
    if (process.env.NODE_ENV !== 'production') {
        middlewares.push(createLogger());
    }

    let store = createStore(
        todoApp,
        applyMiddleware(...middlewares),
        autoRehydrate()
    );

    persistStore(store);

    return store;
};

export default configureStore;

export const getPersistor = ()=> persistor;
