import { createSelector } from 'reselect'
import { getPropIfExists as oGet } from '../util'

export const getHighlightId = (state) => state.highlightId;
export const getTodosById = (state) => state.byId;

export const getHighlightTodo = createSelector(
    [ getHighlightId, getTodosById ],
    (highlightId, todosById) => {
        return todosById[highlightId]
    }
);

export const getUser = state => state.user;
export const getUserProp = (state, prop) => oGet(getUser(state), prop, '');
export const getUserName = state => getUserProp(state, 'name');