import React, { PropTypes } from 'react';

import Menu from './Menu';
import DevToolDisabler from './DevToolDisabler'
import { isProduction } from "../util";

const App = ({children}) => {
    let devDisabler = isProduction()? <DevToolDisabler/>: 'Dev tools are active!';

    return(
        <div>
            {devDisabler}
            <Menu />
            {/*<MuiThemeProvider>*/}
                {/*<AwesomeButton/>*/}
            {/*</MuiThemeProvider>*/}
            <div style={{
                marginTop: '40px',
                marginLeft: '65px',
                marginRight: '65px',
                marginBottom: '20px',
            }}>
                {children}
            </div>
        </div>
    )};

export default App;
