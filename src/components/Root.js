import React, { PropTypes } from 'react';
import { Provider } from 'react-redux';
import { Router, Route, browserHistory, IndexRedirect } from 'react-router';
import functional from 'react-functional'
import * as _ from 'lodash'

import App from './App';
import Setup from './Setup';
import Show from './Show';
import FacebookBrowser from './FacebookBrowser'
import Chat from "./Chat";

const Root = ({ store }) => (
    <Provider store={store}>
        <Router history={browserHistory}>
            <Route path="/" component={App}>
                <IndexRedirect to="setup/" />
                <Route path="/show(/)" component={Show}>
                    <Route path=":filter"/>
                </Route>
                <Route path="/setup(/)" component={Setup}>
                    <Route path=":focusField"/>
                </Route>
                <Route path="/fb(/)" component={FacebookBrowser} />
                <Route path="/chat(/)" component={Chat}>
                    <Route path=":chatGroup"/>
                </Route>
            </Route>
        </Router>
    </Provider>
);

Root.propTypes = {
    store: PropTypes.object.isRequired,
};

Root.componentDidMount = (self) => {
    console.log('process.env.NODE_ENV: ' + (process.env.NODE_ENV || ' not passed to client!'));
    Notification.requestPermission().then(function(result) {
        switch (result) {
            case 'granted':
                console.log('granted');
                break;
            case 'denied':
                console.log('denied');
                break;
            default:
                console.log('something strange happened in Notification.requestPermission');
                break;
        }
        console.log(result);
    });
};

export default functional(Root);
