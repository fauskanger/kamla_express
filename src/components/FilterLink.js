import React, { PropTypes } from 'react';
import { Link, IndexLink } from 'react-router';

const FilterLink = ({ filter, children, rootPath='/show/' }) => {
    const isAll = filter === 'all';
    const activeStyle = {
        textDecoration: 'none',
        color: 'black',
    };
    const to = rootPath + (isAll? '' : filter);
    const allLink = ()=> (<IndexLink to={to} activeStyle={activeStyle}>{children}</IndexLink>);
    const otherLink = ()=>(<Link to={to} activeStyle={activeStyle}>{children}</Link>);
    return(isAll? allLink(): otherLink());
};

FilterLink.propTypes = {
    filter: PropTypes.oneOf(['all', 'completed', 'active']).isRequired,
    children: PropTypes.node.isRequired,
    rootPath: PropTypes.string,
};

export default FilterLink;
