import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import * as _ from 'lodash'

import { getCardImg } from '../api/card'
import byId, * as fromById from '../reducers/byId';
import Todo from './Todo';
import {getRandomTodoId} from "../reducers/index";
import {getUser, getUserProp} from '../selectors'
import {getPropIfExists as oGet, hasProp, renderAsJSON} from '../util'
import { updateUser } from '../actions/user'

let FlipSwitch = ({current, value, id, name, label='', type='checkbox', ...rest}) => {
    return (
        <div key={id}>
            {/*<div className="onoffswitch">*/}
            <Field component={'input'}
                   name={name}
                   type={type}
                //className="onoffswitch-checkbox"
                   id={id}
                   value={value}
                //checked={(current && current==value)? 'selected': undefined}
                //value={value}
                //onChange={()=>console.log(`Changed: ${id}`)}
                   {...rest} />
            <label
                //className="onoffswitch-label"
                htmlFor={id}>
                {label}
                {/*<span className="onoffswitch-inner"></span>*/}
                {/*<span className="onoffswitch-switch"></span>*/}
            </label>
            {/*</div>*/}
        </div>)
};
FlipSwitch.propTypes = {
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    label: PropTypes.string,
    type: PropTypes.oneOf(['checkbox', 'radio']),
    checked: PropTypes.bool,
    onChange: PropTypes.func,
};

let FormField = ({component='input', name, label='', focusField, ...rest})=> {
    let selected = focusField == name? 'selected': '';
    rest = {selected, ...rest};
    return (<div key={name}>
        <Field component={component}
               name={name}
               placeholder={label}
               className="form-control"
               {...rest}/>
    </div>)
};
FormField.propTypes = {
    component: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    label: PropTypes.string,
    onChange: PropTypes.func,
    focusField: PropTypes.string
};

let fields = {
    name: {
        component: 'input',
        type: 'text',
        name: 'name',
        label: 'Name',
    },
    color: {
        component: 'input',
        name: 'color',
        label: 'Color',
        type: 'text'
    },
    description: {
        component: 'textarea',
        type: 'text',
        name: 'description',
        label: 'Description',
    }
};

let Setup = ({ formValues, dispatch, handleSubmit, pristine, initialize, getUser, reset, submitting, params: {focusField} }) => {
    let suits = ['c', 'h', 's', 'd'];
    let ranks = _.range(1,14); // 1,2,...,13
    let cards = [];
    suits.forEach(suit => ranks.forEach(rank => cards.push({rank, suit})));

    let boxTypes = true? 'radio': 'checkbox';

    let iBox = 0;
    let isRadio = ()=> boxTypes == 'radio';
    let nameCount = !isRadio()? ()=>++iBox: ()=>''; // Append incremental number to each field if checkbox, keep name if radio

    let dispatchUserUpdate = data => dispatch(updateUser(data));

    return(
        <div className="container">
            <div>
                <h2>Setup</h2>
            </div>
            <form onSubmit={handleSubmit(dispatchUserUpdate)}>
                <div style={{}}>
                    {Object.keys(fields).map(field=>FormField({focusField, ...fields[field]}))}
                    <button type="submit" className="btn btn-primary"
                            disabled={pristine || submitting}
                            style={{borderRadius: '5px 5px 30px 30px'}}>
                        Save
                    </button>
                    <div className="btn-group btn-group-sm"
                         style={{float: 'right', position: 'relative', top: '0px'}}>
                        <button type="button" className="btn btn-warning"
                                style={{borderRadius: '5px 0px 5px 30px'}}
                                disabled={pristine || submitting}
                                onClick={()=>initialize(getUser())}>
                            Reset Values
                        </button>
                        <button type="button" className="btn btn-danger"
                                style={{borderRadius: '0px 5px 30px 5px'}}
                                disabled={_.isEmpty(formValues) || submitting}
                                onClick={()=>initialize({})}>
                            Clear Values
                        </button>
                    </div>
                </div>
                <div className="form-group" style={{padding: '10px', margin: '20px'}}>
                    {
                        [{
                            id: 'box1',
                            name: 'boxes' + nameCount(),
                            label: 'Butter',
                            type: boxTypes,
                        },{
                            id: 'box2',
                            name: 'boxes' + nameCount(),
                            label: 'Milk',
                            type: boxTypes,
                        },{
                            id: 'box3',
                            name: 'boxes' + nameCount(),
                            label: 'Cheese',
                            type: boxTypes,
                        }].map(flip =>
                            FlipSwitch({
                                value: flip.label,
                                //current: values? values[flip.name]: undefined,
                                ...flip
                            }))
                    }
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-xs-6">
                            <h3>Form data:</h3>
                            {renderAsJSON(formValues)}
                        </div>
                        <div className="col-xs-6">
                            <h3>User data:</h3>
                            {renderAsJSON(getUser())}
                        </div>
                    </div>
                </div>
                {/*<div>*/}
                {/*{cards.map(card => getCardImg(card))}*/}
                {/*</div>*/}
            </form>
        </div>)
};

Setup.propTypes = {
    // todo: PropTypes.shape({
    //     id: PropTypes.string.isRequired,
    //     completed: PropTypes.bool.isRequired,
    //     text: PropTypes.string.isRequired,
    // }),
    // highlightCandidates: PropTypes.object,
    // onClick: PropTypes.func.isRequired,
};

Setup = reduxForm({
    form: 'setup', // a unique name for this form
    destroyOnUnmount: false,
})(Setup);

const formSelector = formValueSelector('setup');
const mapStateToProps = (state, { params }) => {
    return {
        formValues: oGet(state.form.setup, 'values'), //formSelector(state, 'name', 'boxes', 'color')
        initialValues: getUser(state),
        getUser: ()=> getUser(state)
    };
};
Setup = withRouter(connect(
    mapStateToProps
)(Setup));

export default Setup;