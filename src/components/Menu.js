import React, { PropTypes } from 'react';
import { Link, isActive } from 'react-router';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import _ from 'lodash';

import {getPropIfExists as oGet, hasProp, renderAsJSON} from '../util';

const MenuItems = [
    {
        to: '/setup/',
        label: 'Setup'
    },{
        to: '/show/',
        label: 'Show'
    },{
        to: '/fb/',
        label: 'Facebook'
    },{
        to: '/chat/',
        label: 'Chat'
    }
];

let Menu = ({router, params}) => {
    let linkKey = 0;
    return(<div id="menuBar" style={{marginTop: '10px'}}>
        <ul className="nav nav-tabs">
            {_(MenuItems)
                .map(({to, label}) =>
                    <li key={++linkKey} className={router.isActive({pathname: to, params: params})? 'active': ''}>
                        <Link
                            /*activeStyle={{
                                opacity: 0.5,
                                color: '#666',
                                backgroundColor: '#EEE',
                            }}*/
                            to={to}>
                            {label}</Link>
                    </li>)
                .value()}
        </ul>
    </div>)
};


Menu = withRouter(Menu);

export default Menu
