import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import functional from 'react-functional'
import * as _ from 'lodash'

import {getUser as getUserSelector, getUserProp} from '../selectors'
import {getPropIfExists as oGet, hasProp, renderAsJSON} from '../util'


let Chat = ({ dispatch, handleSubmit, pristine, initialize, getUser, reset, submitting, params: {chatGroup} }) => {

    return(
        <div className="container">
            <div>
                <h2>Chat</h2>
                <Link to="/chat/bob">Bob</Link>
            </div>
            <form onSubmit={handleSubmit(()=>false)}>
                <div style={{}}>
                    Let's chat!<br/>
                    Current chatGroup: <strong>{chatGroup||'None'}</strong>.
                    {/*<button type="submit" className="btn btn-primary"*/}
                            {/*disabled={pristine || submitting}*/}
                            {/*style={{borderRadius: '5px 5px 30px 30px'}}>*/}
                        {/*Save*/}
                    {/*</button>*/}
                    {/*<div className="btn-group btn-group-sm"*/}
                         {/*style={{float: 'right', position: 'relative', top: '0px'}}>*/}
                        {/*<button type="button" className="btn btn-warning"*/}
                                {/*style={{borderRadius: '5px 0px 5px 30px'}}*/}
                                {/*disabled={pristine || submitting}*/}
                                {/*onClick={()=>initialize(getUser())}>*/}
                            {/*Reset Values*/}
                        {/*</button>*/}
                        {/*<button type="button" className="btn btn-danger"*/}
                                {/*style={{borderRadius: '0px 5px 30px 5px'}}*/}
                                {/*disabled={_.isEmpty(formValues) || submitting}*/}
                                {/*onClick={()=>initialize({})}>*/}
                            {/*Clear Values*/}
                        {/*</button>*/}
                    {/*</div>*/}
                </div>
            </form>
        </div>)
};

Chat.propTypes = {
    // todo: PropTypes.shape({
    //     id: PropTypes.string.isRequired,
    //     completed: PropTypes.bool.isRequired,
    //     text: PropTypes.string.isRequired,
    // }),
    // highlightCandidates: PropTypes.object,
    // onClick: PropTypes.func.isRequired,
};

Chat = functional(Chat);

Chat = reduxForm({
    form: 'chat', // a unique name for this form
    // destroyOnUnmount: false,
})(Chat);

const mapStateToProps = (state, { params }) => {
    return {
        // formValues: oGet(state.form.setup, 'values'), //formSelector(state, 'name', 'boxes', 'color')
        // initialValues: getUser(state),
        chatGroup: params.chatGroup || 'default',
        getUser: ()=> getUserSelector(state)
    };
};
Chat = withRouter(connect(
    mapStateToProps
)(Chat));

export default Chat;