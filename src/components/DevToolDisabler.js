import React from 'react';

const DevToolDisabler = () =>
    (<script>
        window.__REACT_DEVTOOLS_GLOBAL_HOOK__.inject = function () {}
    </script>);

export default DevToolDisabler;