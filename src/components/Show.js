import React, { PropTypes } from 'react';
import FilterGroup from './FilterGroup';
import AddTodo from './AddTodo';
import VisibleTodoList from './VisibleTodoList';
import Highlight from './Highlight';


let Show = () => {

    return (<div>
        <AddTodo />
        < FilterGroup />
        < VisibleTodoList />
        < Highlight />
    </div>)
};



Show.propTypes = {
    params: PropTypes.shape({
        filter: PropTypes.string,
    }),
};

export default Show;