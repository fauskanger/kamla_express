import React from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm, formValueSelector } from 'redux-form';
import * as _ from 'lodash'

import {getPropIfExists as oGet, hasProp, renderAsJSON} from '../util'

let loggedIn = false;
window.fbAsyncInit = function() {
    FB.init({
        appId      : '470174426515569',
        xfbml      : true,
        version    : 'v2.7'
    });
    FB.AppEvents.logPageView();
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            console.log('Logged in.');
            loggedIn = true;
        } else {
            loggedIn = false;
        }
    });
};

(function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

const myFacebookLogin = () => {
    FB.login(function(){}, {scope: 'publish_actions'});
};

let fields = {
    name: {
        component: 'input',
        type: 'text',
        name: 'pageName',
        label: 'Name of page',
    }
};


let FacebookBrowser = ({ formValues, dispatch, handleSubmit, pristine, initialize, reset, submitting}) => {

    return <div>
        <form onSubmit={handleSubmit(()=>false)}>
            <Field component="button" onClick={myFacebookLogin} name="fbLoginButton">
                {loggedIn? 'Logout': 'Login'}
            </Field>
        </form>
    </div>
};

FacebookBrowser.propTypes = {
    // todo: PropTypes.shape({
    //     id: PropTypes.string.isRequired,
    //     completed: PropTypes.bool.isRequired,
    //     text: PropTypes.string.isRequired,
    // }),
    // highlightCandidates: PropTypes.object,
    // onClick: PropTypes.func.isRequired,
};

const mapStateToProps = (state, { params }) => {
    return {
        formValues: oGet(state.form.facebookBrowser, 'values'), //formSelector(state, 'name', 'boxes', 'color')
    };
};

FacebookBrowser = reduxForm({
    form: 'facebookBrowser', // a unique name for this form
    destroyOnUnmount: false,
})(FacebookBrowser);

const formSelector = formValueSelector('facebookBrowser');
FacebookBrowser = connect(
    mapStateToProps
)(FacebookBrowser);

export default FacebookBrowser;



