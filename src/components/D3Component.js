import React from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm, formValueSelector } from 'redux-form';
import * as _ from 'lodash'

import RenderD3 from '../fiddle/d3test';


let D3Component = ({}) => <script>RenderD3()</script>;

export default D3Component;