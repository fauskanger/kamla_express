import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import byId, * as fromById from '../reducers/byId';
import {getHighlightTodo} from '../selectors'
import Todo from './Todo';
import {getRandomTodoId} from "../reducers/index";
import {randomProperty} from '../util'

let Highlight = ({ todo, highlightCandidates, onClick, dispatch }) => {
    let highlighted = todo == null ?
        false:
        (<p onClick={onClick}>
            <big><strong>{todo.text}</strong></big>
        </p>);
    return(
        <div>
            <button
                onClick={()=>dispatch({
                    type: 'UPDATE_HIGHLIGHT',
                    newHighlightId: randomProperty(highlightCandidates, (id)=>!todo || id!=todo.id).id
                })}>
                Random
            </button>
            {highlighted}
        </div>)
};

Highlight.propTypes = {
    todo: PropTypes.shape({
        id: PropTypes.string.isRequired,
        completed: PropTypes.bool.isRequired,
        text: PropTypes.string.isRequired,
    }),
    highlightCandidates: PropTypes.object,
    onClick: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
    return {
        highlightCandidates: state.byId,
        todo: getHighlightTodo(state),
        onClick: ()=>console.log('Highlighted clicked!'),
    };
};

Highlight = connect(
    mapStateToProps
)(Highlight);

export default Highlight;
