import path from 'path';
import webpack from 'webpack';

export default {
    entry: {
        kamla: './src/index',
        fiddle: './src/fiddle/d3test.js'
    },
    output: {
        path: path.join(__dirname, 'public', 'js'),
        filename: '[name].bundle.js',
        publicPath: '/static/js'
    },
    module: {
        loaders: [{
            test: /\.js$/,
            loaders: ['babel'],
            exclude: /node_modules/,
            include: __dirname,
        }],
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
        })
    ]
}
