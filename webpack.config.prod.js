import webpack from 'webpack'
import commonConfig from './webpack.config.common'

let plugins = commonConfig.plugins || [];

export default {
    ...commonConfig,
    plugins: [
        ...plugins,
        // Optimization plugins: https://webpack.github.io/docs/optimization.html
        new webpack.optimize.UglifyJsPlugin(),
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.optimize.DedupePlugin(),
    ]
};
