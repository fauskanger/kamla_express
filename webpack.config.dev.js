import commonConfig from './webpack.config.common'

export default {
    ...commonConfig,
    devtool: 'eval',
};
